var header = document.getElementsByTagName("header")[0];
var header_nav = header.querySelector("nav");
var sticky = header.querySelector(".menu_up").clientHeight+100;
var mycontent = document.getElementById("content");


jQuery(function() {
  console.log( "ready!" );
  swiperLosMejores();
  fixedHeader();
  
  window.onscroll = function() {
    fixedHeader();
  };
  // Hover effects mosaico trabajos.
  jQuery(".mosaico_trabajos article").hover(function(event){
    jQuery(event.currentTarget.querySelector(".cont_info")).fadeIn("fast")
  });
  jQuery(".mosaico_trabajos article").mouseleave(function(event){
    jQuery(event.currentTarget.querySelector(".cont_info")).fadeOut("fast")
  });
  
  jQuery("header nav").on('click',function() {
      if(getCurrentWidth() < 768){
        jQuery(this.querySelector("ul")).toggleClass("showMenu");  
      }
  });


});

function fixedHeader() {
  if (window.pageYOffset > sticky) {
    if(window.innerWidth > 576){
      jQuery(header.querySelector(".menu_up")).slideUp(200);
    }
    header.classList.add("on_sticky")
    header_nav.classList.add("sticky");
  } else {
    if(window.innerWidth > 576){
      jQuery(header.querySelector(".menu_up")).slideDown(200);
    }
    header_nav.classList.remove("sticky");
    header.classList.remove("on_sticky");
  }
}

// VALIDATION FORMS. 
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function swiperLosMejores() {
  var objSwiper = document.querySelector("section.los-mejores");
  if(objSwiper == null ){
    return
  }
  var swiper = new Swiper(objSwiper.querySelector(".swiper-los-mejores"), {
    pagination: {
      el: objSwiper.querySelector('.swiper-pagination'),
      type: 'fraction',
    },
    navigation: {
      nextEl: objSwiper.querySelector('.swiper-button-next'),
      prevEl: objSwiper.querySelector('.swiper-button-prev'),
    },
  });
}

function getCurrentWidth(){
  return document.body.clientWidth;
}