#!/bin/bash
# como ejecutar el script .
# abri consola git bash en la carpeta "source" 
# para crear un modulo ejecutamos :
# sh create_page.sh "nombre_pagina"

echo "++++++++++ INICIO SCRIPT create_page ++++++++++"
echo "Nombre de la pagina, ej: page_home"
read module_name

if [ -d $module_name ]; then
    echo "Hay una carpeta con ese nombre."
    sh create_page.sh 
    exit
fi 

echo "Elegir layout de la pagina"
echo "1 (one-column)"
echo "2 (two-column-right)"
echo "3 (two-column-left)"
echo "******* "
read op_layout 

if [ $op_layout -eq '1' ]; then
    module_layout="one-column"
fi
if [ $op_layout -eq '2' ]; then
    module_layout="two-column-right"
fi
if [ $op_layout -eq '3' ]; then
    module_layout="two-column-left"
fi

echo "******* ¿ Desea importar el modulo ? *******"

echo "Nombre: "$module_name
echo "Layout: "$module_layout

echo "1 (Si)"
echo "2 (No)"
echo "******* "
read validar
if [ $validar -eq '2' ]; then
    echo "Fin del programa. "
    exit
fi
echo "iniciamos creación de la pagina: "$module_name

# mkdir $module_name 
touch $module_name".pug" 
# escribimos en fichero index.pug 
echo '
extends ../includes/pug/layout/'$module_layout'.pug
block content 
    h1 Pagina: '$module_name >> $module_name".pug"



echo "++++++++++ INICIO SCRIPT ++++++++++"

    