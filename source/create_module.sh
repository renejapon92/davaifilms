#!/bin/bash
# como ejecutar el script .
# abri consola git bash en la carpeta "source" 
# para crear un modulo ejecutamos :
# sh create_page.sh "nombre_pagina"

#functions

function getLayout(){
    if [ $op_layout -eq '1' ]; then
        module_layout="one-column"
    fi
    if [ $op_layout -eq '2' ]; then
        module_layout="two-column-right"
    fi
    if [ $op_layout -eq '3' ]; then
        module_layout="two-column-left"
    fi
    echo $module_layout
}


function printLayout(){
    if [ $op_layout -eq '1' ]; then
        echo '
extends ../includes/pug/layout/'$1'.pug
block content 
    h1 Pagina: '$module_name >> $module_name"/index.pug"
    
    fi
    if [ $op_layout -eq '2' ]; then
        echo '
extends ../includes/pug/layout/'$1'.pug
block content 
    h1 Pagina: '$module_name'
block aside 
    h1 Pagina: '$module_name >> $module_name"/index.pug"

    fi
    if [ $op_layout -eq '3' ]; then
        echo '
extends ../includes/pug/layout/'$1'.pug
block aside
    h2 aside '$module_name'
block content 
    h1 Pagina: '$module_name >> $module_name"/index.pug"

    fi
    echo $module_layout
}


function getLayout(){
    if [ $op_layout -eq '1' ]; then
        module_layout="one-column"
    fi
    if [ $op_layout -eq '2' ]; then
        module_layout="two-column-right"
    fi
    if [ $op_layout -eq '3' ]; then
        module_layout="two-column-left"
    fi
    echo $module_layout
}


#functions



echo "++++++++++ INICIO SCRIPT create_module ++++++++++"
echo "Nombre de la pagina, ej: module_carousel"
read module_name

if [ -d $module_name ]; then
    echo "Hay una carpeta con ese nombre."
    sh create_module.sh 
    exit
fi 

echo "Elegir layout de la pagina"
echo "1 (one-column)"
echo "2 (two-column-right)"
echo "3 (two-column-left)"
echo "******* "
read op_layout 

echo "mi laytou es: $(getLayout)" 


echo "******* ¿ Desea importar el modulo ? *******"

echo "Nombre: "$module_name
echo "Layout: "$(getLayout)

echo "1 (Si)"
echo "2 (No)"
echo "******* "
read validar
if [ $validar -eq '2' ]; then
    echo "Fin del programa. "
    exit
fi
echo "iniciamos creación de la pagina: "$module_name



s_seccion=(particulares empresa banca)
s_media_query=(mobile 576min 768min 1200min 1440min)
__separador__='/'
ext_estilos='.scss'
ext_pug='.pug'

mkdir $module_name $module_name"/sass" 
touch $module_name"/index.pug" 
touch $module_name"/"$module_name".pug" 

# Escribimos tipo de layout
printLayout $(getLayout)

for query in "${s_media_query[@]}"; 
do 
    # Creamos ficheros scss.
    touch $module_name$__separador__'sass'$__separador__$query$ext_estilos
    # insertamos imports.
    echo '
@import "../../../'$module_name'/sass/'$query$ext_estilos'"; ' >> './includes/sass/general/'$query$ext_estilos
done

#Escribimos nombre del modulo en index.html ç
# echo '<br/><a href="'$module_name'">'$module_name'</a>' >> '../public/index.html'

echo "++++++++++ INICIO SCRIPT ++++++++++"