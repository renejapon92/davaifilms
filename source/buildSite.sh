#!/bin/bash

#generamos lista librerias
echo "generando fichero librerias.txt"
ls includes/librerias > librerias.txt 

# lista de comandos.
comandos=(doJS doCSS doFonts doLibrerias doImages)

for comando in ${comandos[*]}
do
    #Ejecutamos comandos
	gulp $comando
    #Librerias.
    if [ $comando = 'doLibrerias' ]; 
    then
        IFS=$'\r\n' GLOBIGNORE='*' command eval  'librerias=($(cat librerias.txt))'
        # compilamos librerias"
        for libreria in "${librerias[@]}"; 
        do 
            echo "current librerias: "$libreria
            gulp doLibrerias --type "css" --name $libreria 
            gulp doLibrerias --type "js" --name $libreria 
            gulp doLibrerias --type "webfonts" --name $libreria 
        done
    fi
done

# Compilamos web_completa
gulp doHTML