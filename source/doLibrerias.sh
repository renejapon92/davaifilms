#!/bin/bash

echo "Creando fichero con librerias..."
ls includes/librerias > librerias.txt 

IFS=$'\r\n' GLOBIGNORE='*' command eval  'librerias=($(cat librerias.txt))'

# compilamos librerias"
for libreria in "${librerias[@]}"; 
do 
    echo "current librerias: "$libreria
    gulp doLibrerias --type "css" --name $libreria 
    gulp doLibrerias --type "js" --name $libreria 
    gulp doLibrerias --type "webfonts" --name $libreria 
done