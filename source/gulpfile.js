
var gulp = require('gulp'), 
gulp_pug = require('gulp-pug'),
sass = require('gulp-sass'),
cleanCSS = require('gulp-clean-css'),
sourcemaps = require('gulp-sourcemaps'),
cssbeautify = require('gulp-cssbeautify'),
rename = require('gulp-rename'),
browserSync = require('browser-sync').create(),
debug = require('gulp-debug');
gulp.task('default', () =>{
    var commad_list = 'Lista de comandos \n 1. serve: servidor \n 1. doHtml: compila html \n 1. doCSS: compila sass \n 1. doImages: mueve imagenes a public \n 1. doFonts: movemos fuentes a public ' ;
    console.log(commad_list);
});

function watcher(){
	browserSync.init({
		server: {
			baseDir: "../public/",
		}
	});
	gulp.watch(["./**/*.pug","./*.pug"],makeHTML)
	gulp.watch(["../public/*.html","../public/css/*.css"]).on('change', browserSync.reload );
	gulp.watch(["./includes/js/*.js"],['doJS']).on('change', browserSync.reload );
	gulp.watch(["./**/**/*.scss","../**/sass/*.scss"],['doCSS'] ).on('change', browserSync.reload );
}
 
function makeHTML(pathFile){
	console.log("Ejecutamos tarea")
	console.log(pathFile.path)
	var pageToCompile = pathFile.path.split("\\")[pathFile.path.split("\\").length-1]
	var folderContainer = pathFile.path.split("\\")[pathFile.path.split("\\").length-2]
	console.log("modulo: "+pageToCompile)
	console.log("folder container: "+folderContainer)
	if( folderContainer == "web_completa" ){
		console.log("compilando: "+pageToCompile)
		return gulp.src(['./web_completa/'+pageToCompile] )
		.pipe(gulp_pug({ pretty: true }) )
		// .pipe(rename({
        //     extname: '.php'
        // }))
		.pipe(gulp.dest('../public/'));
	}
}

// COMPILACION DE JAVA SCRIPT
gulp.task('doHTML', () => {
	return gulp.src(['./web_completa/*'])
	.pipe(gulp_pug(
		{ 
			pretty : true 
		}
	))
	.pipe(gulp.dest('../public/'));
});


// COMPILACION DE JAVA SCRIPT
gulp.task('doJS', () => {
	return gulp.src(['./includes/js/*.js'])
	.pipe(gulp.dest('../public/lib/js/'));
});


// COMPILACION DE TIPOGRAFIAS
gulp.task('doFonts', () => {
	return gulp.src(['./includes/sass/fonts/*.*'])
	.pipe(gulp.dest('../public/skin/css/fonts'));
});

// COMPILACION DE LIBRERIAS
gulp.task('doLibrerias', () => {
	const arg = (argList => {
		let arg = {}, a, opt, thisOpt, curOpt;
		for (a = 0; a < argList.length; a++) {
		  thisOpt = argList[a].trim();
		  opt = thisOpt.replace(/^\-+/, '');
		  if (opt === thisOpt) {
			if (curOpt) arg[curOpt] = opt;
			curOpt = null;
		  }
		  else {
			curOpt = opt;
			arg[curOpt] = true;
		  }
		}
		return arg;
	})(process.argv);
	console.log(arg)
	return gulp.src(['./includes/librerias/'+arg['name']+"/"+arg['type']+"/*.*" ])
	.pipe(debug({title: 'procesando:'}))
	.pipe(gulp.dest('../public/lib/librerias/'+arg['name']+"/"+arg['type']  ));

});

// COMPILACION DE JS
gulp.task('doImages', () => {
	return gulp.src(['./includes/image/*.*'])
	.pipe(gulp.dest('../public/media/image'));
});

// CREAMOS CSS GENERAL.
gulp.task('doCSS', () => {
	return gulp.src(['./includes/sass/styles.scss'])
	.pipe(sass({ pretty: true }))
	.pipe(cssbeautify())
	// .pipe(cleanCSS({compatibility: 'ie8'}))

	.pipe(sourcemaps.write())
	.pipe(gulp.dest('../public/skin/css'));
});

exports.watch = watcher;